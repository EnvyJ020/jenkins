stage{'yumreposd':}
Stage['yumreposd'] -> Stage['main']



class {'yum':
    extrarepo => ['epel','remi','remi_php56'],
    stage => 'yumreposd'
}

exec {'yum install -y ant':
    path => "/usr/bin:/usr/sbin:/bin",
    creates => "/usr/bin/ant",
    timeout => 0,
    before => Class['jenkins']
}


exec {'yum install -y nodejs':
    path => "/usr/bin:/usr/sbin:/bin",
    timeout => 0,
    creates => "/usr/bin/node",
} ->
    exec {'yum install -y npm':
        path => "/usr/bin:/usr/sbin:/bin",
        timeout => 0,
        creates => "/usr/bin/npm"
    } ->
        exec {'npm install -g bower':
            path => "/usr/bin:/usr/sbin:/bin",
            timeout => 0,
            creates => "/usr/bin/bower"
        }



# [CREDENCIALES JENKINS]
    $fullname = 'Ivan Fernandez Villar'
    $username = 'envyj020'
    $email = 'ifervi@hotmail.com'
    $password = 'benqt906'
# [FIN CREDENCIALES JENKINS]



class {'jenkins':
    require => Class['apache'] 
}
    jenkins::user { $username:
      full_name => $fullname,
      email    => $email,
      password => $password
    } ->
        file {['/var/lib/jenkins/jobs','/var/lib/jenkins/jobs/php-template']:
            ensure => 'directory',
            owner => 'jenkins',
            group => 'jenkins',
            mode => 755
        } ->
            wget::fetch {'Descarga de PHP TEMPLATE para Jenkins':
                source => 'https://raw.github.com/sebastianbergmann/php-jenkins-template/master/config.xml',
                destination => '/var/lib/jenkins/jobs/php-template/config.xml',
                timeout => 0,
                execuser => 'jenkins',
                verbose => false
            }

jenkins::plugin { ['git','crap4j','checkstyle','cloverphp','dry','htmlpublisher','jdepend','plot','pmd','violations','xunit']:}


class {'composer':
  target_dir => '/usr/bin',
  user => 'root',
  auto_update => false,
}

include apache

include iptables

include php

php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'tidy', 'pecl-memcache', 'soap', 'pdo' ]: }