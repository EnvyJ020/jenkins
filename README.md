#Entorno de desarrollo de Integración continua con Jenkins
-----------------------------------------------------------
##Instalación/Configuración:
----------------------------
* Una vez clonado nuestro repositorio, entramos en nuestra copia local y nos dirigimos a environment/modules/, una vez dentro ejecutar :
```bash
git submodule init
git submodule update
```
* Por defecto utiliza la box `puphpet/centos65-x64` [centOS 6.5 x86_64] la que nos genera puphpet, utilizarla si la teneis sino utilizad un centOS 6.5 con puppet integrado ejecutando:
```bash
vagrant box add :name: http://puppet-vagrant-boxes.puppetlabs.com/centos-65-x64-virtualbox-puppet.box
```
* Editar el archivo environment/config.yaml y modificad el nombre de la box por el que le hayas puesto al añadirla
* Entrar en environment/manifests/site.pp y modificad el apartado de `CREDENCIALES JENKINS`, introducid vuestras propias credenciales
* Editar vuestro fichero hosts del sistema operativo anfitrión y adjudicadle una DNS a `192.168.33.20` o edita la IP de la maquina virtual en environment/config.yaml si esta ya está siendo utilizada.
* Listo!
```bash
vagrant up
```
## Configuración Jenkins
------------------------
###Primero procederemos a asegurar nuestro entorno para que unicamente sea accesible por nosotros:
* `Administrar Jenkins` > `Configuración global de seguridad` : seleccionar el checkbox `'Activar seguridad'` y seleccionar en Mark Formatter la opción `'RAW Html'`.
* En el Apartado de Seguridad activad `'Usar base de datos de Jenkins'` y nos aseguramos que esté desactivada la opción 'Permitir que los usuarios se registren'.
* Seleccionamos `'Estrategia de seguridad para el proyecto'` y añadimos el nombre de usuario que previamente hemos configurado en environment/manifests/site.pp `$username` y le activamos todos los permisos.
* Guardamos los cambios y si Jenkins no se reinicia automaticamente, nos logueamos e introducimos la siguiente url `(jenkins_url):8080/restart` para que se apliquen los permisos y configuraciones de nuestra cuenta.
* Antes de proceder con la compilación del proyecto, necesitamos instalar `GIT Plugin` y ademas editar /etc/php.ini y setear `date.timezone = "Europe/Madrid"`.
Hecho! nuestro sistema de CI solo es accesible por nuestra cuenta de Jenkins y lista para lanzar bolitas azules.
